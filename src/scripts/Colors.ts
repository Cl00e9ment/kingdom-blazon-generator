/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

// @ts-ignore
import colorsDefinitions from '../assets/colors.json';
import Color from './Color';

interface ColorDefinition {
	name: string,
	rgb: number[],
}

const _colors: Color[] = [];
const _unprefixedColors: Color[] = [];

for (const def of colorsDefinitions as ColorDefinition[]) {
	_colors.push(new Color(def.rgb, def.name));
}

for (const color of _colors) {
	let isPrefixed = false;
	for (const otherColor of _colors) {
		if (color.name.length > otherColor.name.length && color.name.substring(0, otherColor.name.length) === otherColor.name) {
			isPrefixed = true;
			break;
		}
	}
	if (!isPrefixed) {
		_unprefixedColors.push(color);
	}
}

export const colors: readonly Color[] = _colors;
export const unprefixedColors: readonly Color[] = _unprefixedColors;
