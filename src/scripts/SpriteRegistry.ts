/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import Game from './Game';
import { getSelectedGame } from './GameSelector'
import Sprite from './Sprite';
import { downloadImage } from './Utils';
// @ts-ignore
import ordinariesDefinitions from '../assets/ordinaries.json';
// @ts-ignore
import chargesDefinitions from '../assets/charges.json';
// @ts-ignore
import spriteSheetUrl from 'url:../assets/sprites.png';

interface AssetDefinition {
	name: string,
	games: string[],
	x: number,
	y: number,
	width: number,
	height: number,
}

export default class SpriteRegistry {

	public static ordinaries: SpriteRegistry;
	public static charges: SpriteRegistry;

	public static async load(): Promise<void> {
		const spriteSheet = await downloadImage(spriteSheetUrl);
		SpriteRegistry.ordinaries = new SpriteRegistry(spriteSheet, ordinariesDefinitions);
		SpriteRegistry.charges = new SpriteRegistry(spriteSheet, chargesDefinitions);
	}

	private spritesByGame = {} as {[key: string]: Sprite[]};
	private allSprites = [] as Sprite[];

	private constructor(spriteSheet: HTMLImageElement, definitions: AssetDefinition[]) {
		Game.all.forEach((game) => {
			this.spritesByGame[game.id] = [];
		});
		for (const def of definitions) {
			const sprite = new Sprite();
			sprite.loadFromSpriteSheet(spriteSheet, def.x, def.y, def.width, def.height);
			sprite.name = def.name;
			this.allSprites.push(sprite);
			for (const gameId of def.games) {
				this.spritesByGame[gameId].push(sprite);
			}
		}
	}

	public getSprites(game?: Game): Sprite[] {
		return game ? this.spritesByGame[game.id] : this.allSprites;
	}

	public getGamesWhereSpriteIsPresent(sprite: Sprite): Game[] {
		return Game.all.filter((g) => this.spritesByGame[g.id].includes(sprite));
	}

	public getSubstituteIfNeeded(sprite: Sprite): Sprite {
		const gamesWhereSpriteIsPresent = this.getGamesWhereSpriteIsPresent(sprite);
		const currentGame = getSelectedGame();
		if (gamesWhereSpriteIsPresent.includes(currentGame)) {
			return sprite;
		} else {
			const availableSprites = this.getSprites(currentGame);
			if (availableSprites.length === 0) {
				return null;
			}
			if (!sprite) {
				return availableSprites[0];
			}
			for (const availableSprite of availableSprites) {
				if (availableSprite.name === sprite.name) {
					return availableSprite;
				}
			}
			return availableSprites[availableSprites.length - 1];
		}
	}
}
