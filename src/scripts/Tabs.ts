/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import { onSelectedGameChanged } from './GameSelector';
import { setState } from './Utils';
import Game from './Game';

export type SelectedTabChangedListener = (oldIndex: number, newIndex: number) => void;

const tabs = document.querySelectorAll('nav li') as NodeListOf<HTMLLIElement>;
const listeners: SelectedTabChangedListener[] = [];
let selectedTabIndex = NaN;

onSelectedGameChanged((selectedGame) => {
	if (selectedGame === Game.TWO_CROWNS_SHOGUN && selectedTabIndex === 2) {
		setSelectedTabIndex(1);
	}
	tabs[2].style.display = selectedGame === Game.TWO_CROWNS_SHOGUN ? 'none' : '';
});

for (let i = 0; i < tabs.length; i++) {
	tabs[i].addEventListener('click', () => {
		setSelectedTabIndex(i);
	});
}

export function setSelectedTabIndex(index: number) {
	if (selectedTabIndex === index) return;
	const oldIndex = selectedTabIndex;
	selectedTabIndex = index;
	for (let i = 0; i < tabs.length; i++) {
		setState(tabs[i], i === index);
	}
	for (const listener of listeners) {
		listener(oldIndex, index);
	}
}

export function onSelectedTabChanged(listener: SelectedTabChangedListener) {
	listeners.push(listener);
}
