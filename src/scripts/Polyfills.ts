import 'regenerator-runtime/runtime';
import 'promise-polyfill/src/polyfill';
import 'classlist-polyfill';

if (!Array.prototype.includes) {
	Array.prototype.includes = function (searchElement: any, fromIndex?: number) {
		return this.indexOf(searchElement, fromIndex) != -1;
	}
}

export function setImageSmoothing(ctx: CanvasRenderingContext2D, value: boolean) {
	for (const property of ['mozImageSmoothingEnabled', 'webkitImageSmoothingEnabled', 'msImageSmoothingEnabled', 'imageSmoothingEnabled']) {
		if (ctx[property] !== undefined) {
			ctx[property] = value;
		}
	}
}
