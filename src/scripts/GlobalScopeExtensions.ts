/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

declare global {
	interface Array<T> {
		randomIndex: () => number;
		randomElement: () => T;
		indexOfNearest: (search: T, distanceFn: (a: T, b: T) => number) => number;
		sort2D: (xCompareFn?: (a: any, b: any) => number, yCompareFn?: (a: any, b: any) => number) => T[];
	}
}

Array.prototype.randomIndex = function() {
	return Math.floor(Math.random() * this.length);
};

Array.prototype.randomElement = function() {
	return this[this.randomIndex()];
};

Array.prototype.indexOfNearest = function(search, distanceFn) {
	let index = -1;
	let distance: number;
	let smallestDistance = Number.MAX_VALUE;
	for (let i = 0; i < this.length; i++){
		distance = distanceFn(search, this[i]);
		if (distance === 0) return i;
		if (distance < smallestDistance){
			smallestDistance = distance;
			index = i;
		}
	}
	return index;
}

Array.prototype.sort2D = function(xCompareFn, yCompareFn) {
	if (this.length === 0) return [];
	this.sort(yCompareFn);
	const size = Math.floor(Math.sqrt(this.length));
	let xySorted = [];
	while (this.length > 0) {
		xySorted = xySorted.concat(this.splice(0, size).sort(xCompareFn));
	}
	this.push(...xySorted);
	return this;
};

export function loadGlobalScopeExtensions() {
	// the only fact to import and use this function will load this module
}
