/* Copyright 2019, 2020 Clément Saccoccio */

/*
This file is part of Kingdom Blazon Generator.

Kingdom Blazon Generator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Kingdom Blazon Generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import Color from './Color';

type SelectedColorChangedListener = (color: Color) => void;

export default class ColorPicker {

	private colors: Color[];
	private selectedColor: Color;
	private listeners: SelectedColorChangedListener[] = [];

	public constructor(mountPoint: HTMLDivElement, colors: readonly Color[]) {
		this.colors = colors.slice();
		this.colors.sort2D(Color.compareLuminance, Color.compareHue);
		mountPoint.classList.add('color-picker');
		for (const color of this.colors){
			const tile = document.createElement('div');
			tile.style.backgroundColor = color.cssName;
			tile.title = color.name;
			tile.addEventListener('click', () => this.select(color));
			mountPoint.appendChild(tile);
		}
	}

	public select(color: Color) {
		if (this.selectedColor !== color) {
			this.selectedColor = color;
			for (const listener of this.listeners) {
				listener(color);
			}
		}
	}

	public selectRandom() {
		this.select(this.colors.randomElement());
	}

	public onSelectedColorChanged(listener: SelectedColorChangedListener) {
		this.listeners.push(listener);
	}
}
