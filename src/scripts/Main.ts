/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import './Polyfills';
import { initClipboardButton } from './ClipboardButton';
import { loadGlobalScopeExtensions } from './GlobalScopeExtensions';
import SpriteRegistry from './SpriteRegistry';
import { createGui } from './GUI';
import { getSelectedGame, onSelectedGameChanged } from './GameSelector'
import { displayError } from './ErrorHandler'
	
async function main() {
	loadGlobalScopeExtensions();
	await SpriteRegistry.load();
	initClipboardButton();
	createGui();

	const element = document.getElementsByTagName('main')[0];
	onSelectedGameChanged((selectedGame, previousSelectedGame) => {
		element.classList.remove(previousSelectedGame.id.toLowerCase());
		element.classList.add(selectedGame.id.toLowerCase());
	});
	element.classList.add(getSelectedGame().id.toLowerCase());

	document.body.classList.remove('loading');
}

try {
	main().catch(displayError);
} catch (e) {
	// If promises don't work a try catch block is necessary.
	displayError(e);
}
