# Asset Generator

Extracts assets and metadata from game files to construct [Kingdom Blazon Generator's assets](../src/assets/).

## How to use

1. Extract game files with [Asset Ripper](https://github.com/AssetRipper/AssetRipper).
   1. `File` > `Open Folder` > `~/.local/share/Steam/steamapps/common/Kingdom Two Crowns`
   2. `Export` > `Export All Files` > `~/Downloads/KingdomTwoCrowns` > `Export Unity Project`
2. Put the extracted `KingdomTwoCrowns` directory here.
3. Go to [project root](../).
4. Run the generator:

```
pnpm install
pnpm generate-assets
```

## Follow-up

- Need to be updated:
  - [Game.ts](../src/scripts/Game.ts)
  - [main.css](../src/style/main.css)
