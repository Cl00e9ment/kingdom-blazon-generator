import { readFileSync, writeFileSync, opendirSync, Dirent } from 'fs'
import { parse as parseYaml } from 'yaml'
import sharp, { OverlayOptions } from 'sharp'
import { ShieldData, ShieldDataSprite, Sprite, SpriteMeta } from './kingdom-types'

const games: Game[] = [
	{
		id: 'TC',
		name: 'Two Crowns',
		biome: 'OakAndBirch',
	},
	{
		id: 'TCS',
		name: 'Two Crowns Shogun',
		biome: 'Bamboo',
	},
	{
		id: 'TCDL',
		name: 'Two Crowns Dead Lands',
		biome: 'Deadlands',
	},
	{
		id: 'TCNL',
		name: 'Two Crowns Norse Lands',
		biome: 'Norselands',
	},
	{
		id: 'TCCO',
		name: 'Two Crowns Call of Olympus',
		biome: 'Greece',
	},
]

const spriteAtlasFileName = 'sactx-0-2048x1024-Uncompressed-buildings-common-4dbea335.png'

interface Game {
	id: string,
	name: string,
	biome: string,
}

interface AssetDefinition {
	name: string,
	games: string[],
	x: number,
	y: number,
	width: number,
	height: number,
}

const charges: Record<string, AssetDefinition> = {};
const ordinaries: Record<string, AssetDefinition> = {};

main().catch(e => console.error(e));

async function main() {
	detectInstalledGame();
	loadChargesAndOrdinaries();
	await generateAssets();
	generateColorList();
}

function detectInstalledGame() {
	const biomeDir = opendirSync(__dirname + '/KingdomTwoCrowns/ExportedProject/Assets/Resources/biome/shielddata');
	let dirent: Dirent | null;

	let gamesToFind = Array.from(games)

	while ((dirent = biomeDir.readSync()) !== null) {
		if (!dirent.isFile() || dirent.name.endsWith('.meta')) {
			continue;
		}

		const biome = dirent.name.replace('ShieldData.asset', '');
		const foundGame = gamesToFind.find(g => g.biome === biome);

		if (foundGame) {
			console.log(`found biome "${biome}" from game "${foundGame.name}"`);
			gamesToFind = gamesToFind.filter(g => g !== foundGame);
		} else {
			console.log(`found biome "${biome}" from unknown game (new unknown DLC installed?)`);
		}
	}

	for (const game of gamesToFind) {
		console.log(`no biome found for game ${game.name} (DLC not installed?)`);
	}

	biomeDir.closeSync();
}

function loadChargesAndOrdinaries() {
	for (const game of games) {
		const path = `${__dirname}/KingdomTwoCrowns/ExportedProject/Assets/Resources/biome/shielddata/${game.biome}ShieldData.asset`;
		const data = readYamlFile(path) as ShieldData;
		parseShieldDataSprites(game.id, data.MonoBehaviour.emblems, charges);
		parseShieldDataSprites(game.id, data.MonoBehaviour.patterns, ordinaries);
	}

	function parseShieldDataSprites(gameId: string, shieldsData: ShieldDataSprite[], result: Record<string, AssetDefinition>) {
		for (const shieldData of shieldsData) {
			if (shieldData.description == 'none') {
				continue;
			}

			const assetDef = result[shieldData.sprite.guid] ?? {
				name: shieldData.description,
				games: [],
				x: NaN,
				y: NaN,
			};

			assetDef.games.push(gameId);

			// ad-hoc way to handle Kingdom New Lands
			if (gameId === 'TC' && !['a tortoise', 'a bald eagle', 'a ram', 'a sheep', 'a greed'].includes(shieldData.description)) {
				assetDef.games.push('NL');
			}

			result[shieldData.sprite.guid] = assetDef;
		}
	}
}

async function generateAssets() {
	const spriteWidth = 25;
	const spriteHeight = 33;
	const finalSpriteSheetWidth = 512;
	let finalSpriteSheetHeight = spriteHeight;
	let finalSpriteSheetNextX = 0;
	let finalSpriteSheetNextY = 0;
	const sprites: OverlayOptions[] = [];

	const assetsGuid = Object.keys(charges).concat(Object.keys(ordinaries));

	const spriteSheetPath = __dirname + '/KingdomTwoCrowns/ExportedProject/Assets/Texture2D/' + spriteAtlasFileName;

	const spritesPath = __dirname + '/KingdomTwoCrowns/ExportedProject/Assets/Sprite';
	const spritesDir = opendirSync(spritesPath);
	let dirent: Dirent | null;

	while ((dirent = spritesDir.readSync()) !== null) {
		if (!dirent.isFile() || !dirent.name.startsWith('banner') || !dirent.name.endsWith('.meta')) {
			continue;
		}

		const spriteGuid = (readYamlFile(dirent.path + '/' + dirent.name) as SpriteMeta).guid;
		if (!assetsGuid.includes(spriteGuid)) {
			continue;
		}

		const isEmblem = dirent.name.includes("emblem");
		const spriteRect = (readYamlFile(dirent.path + '/' + dirent.name.replace('.meta', '')) as Sprite).Sprite.m_Rect;

		spriteRect.x      = Math.round(spriteRect.x);
		spriteRect.y      = Math.round(spriteRect.y);
		spriteRect.width  = Math.round(spriteRect.width);
		spriteRect.height = Math.round(spriteRect.height);

		// reverse y axis
		spriteRect.y = 1024 - spriteRect.y - spriteRect.height;

		// remove margins
		spriteRect.width  = spriteWidth;
		spriteRect.height = spriteHeight;
		spriteRect.x += 3;
		spriteRect.y += 7;

		const buffer = await sharp(spriteSheetPath)
			.extract({
				left: spriteRect.x,
				top: spriteRect.y,
				width: spriteRect.width,
				height: spriteRect.height,
			})
			.raw()
			.toBuffer();

		for (let i = 0; i < buffer.length; i += 4) {
			const is_primarry = buffer[i + 3] === 208 || buffer[i + 3] === 209;
			const shadowed = buffer[i] < 255;

			buffer[i    ] = is_primarry ? 255 : 0;
			buffer[i + 1] = !is_primarry && !isEmblem ? 255 : 0;
			buffer[i + 2] = !is_primarry && isEmblem ? 255 : 0;
			buffer[i + 3] = buffer[i + 3] === 0 ? 0 : 255;

			if (shadowed) {
				buffer[i    ] = 255 - buffer[i    ];
				buffer[i + 1] = 255 - buffer[i + 1];
				buffer[i + 2] = 255 - buffer[i + 2];
			}
		}

		sprites.push({
			input: await sharp(buffer, { raw: {
					width: spriteRect.width,
					height: spriteRect.height,
					channels: 4,
				}})
				.png()
				.toBuffer(),
			left: finalSpriteSheetNextX,
			top: finalSpriteSheetNextY,
		});

		const assetDefinition = (spriteGuid in charges ? charges : ordinaries)[spriteGuid];
		assetDefinition.x = finalSpriteSheetNextX;
		assetDefinition.y = finalSpriteSheetNextY;
		assetDefinition.width = spriteRect.width;
		assetDefinition.height = spriteRect.height;

		// move to next sprite shee line if end reached
		finalSpriteSheetNextX += spriteWidth;
		if (finalSpriteSheetNextX + spriteWidth > finalSpriteSheetWidth) {
			finalSpriteSheetNextX = 0;
			finalSpriteSheetNextY += spriteHeight;
			finalSpriteSheetHeight += spriteHeight;
		}
	}

	spritesDir.closeSync();

	await sharp({
		create: {
			width: finalSpriteSheetWidth,
			height: finalSpriteSheetHeight,
			channels: 4,
			background: { r: 0, g: 0, b: 0, alpha: 0 },
		},
	})
	.composite(sprites)
	.toFile(__dirname + '/../src/assets/sprites.png');

	writeYamlFile(__dirname + '/../src/assets/charges.json', Object.values(charges));
	writeYamlFile(__dirname + '/../src/assets/ordinaries.json', Object.values(ordinaries));
}

function generateColorList() {
	const scriptPath = __dirname + '/KingdomTwoCrowns/ExportedProject/Assets/Scripts/Assembly-CSharp/CoatOfArms.cs'
	const script = readFileSync(scriptPath, 'utf-8');

	const colors: {
		name: string,
		rgb: [number, number, number]
	}[] = [];

	for (const line of script.split('\n')) {
		const match = line.match(/_colors\["(.*)"\] = "(..)(..)(..)"/);
		if (!match) {
			continue;
		}
		colors.push({
			name: match[1],
			rgb: [
				Number.parseInt(match[2], 16),
				Number.parseInt(match[3], 16),
				Number.parseInt(match[4], 16),
			],
		})
	}

	writeYamlFile(__dirname + '/../src/assets/colors.json', colors);
}

function readYamlFile(path: string): any {
	return parseYaml(
		readFileSync(path, 'utf-8')
		.replace(
			/^%YAML 1\.1\n(?:%TAG !u! tag:unity3d.com,\d+:\n)?---(?: !u!\d+ &\d+)?/,
			'%YAML 1.2\n---'
		)
	);
}

function writeYamlFile(path: string, data: any) {
	writeFileSync(path, JSON.stringify(data), 'utf-8');
}
