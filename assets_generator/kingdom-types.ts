export interface ShieldDataSprite {
	sprite: {
		guid: string,
	},
	description: string,
}

export interface ShieldData {
	MonoBehaviour: {
		patterns: ShieldDataSprite[],
		emblems: ShieldDataSprite[],
	},
}

export interface SpriteMeta {
	guid: string,
}

export interface Sprite {
	Sprite: {
		m_Rect : {
			x: number,
			y: number,
			width: number,
			height: number,
		},
	},
}
