# Kingdom Blazon Generator

Generate your custom blazon for [Kingdom New Lands](https://www.kingdomthegame.com/kingdom-new-lands/) and [Kingdom Two Crowns](https://www.kingdomthegame.com/kingdom-two-crowns/) (including Shogun and Dead Lands editions)!

## Where

[https://cl00e9ment.gitlab.io/kingdom-blazon-generator](https://cl00e9ment.gitlab.io/kingdom-blazon-generator)

## Licence

[GNU GPL 3.0 or later](https://www.gnu.org/licenses/agpl.txt)

## Contact

[Clément Saccoccio](mailto:clem.saccoccio@protonmail.com)

## Special thanks

The [Kingdom community](https://kingdomthegame.fandom.com/wiki/Blazons), especially [SapadorCastelo](https://kingdomthegame.fandom.com/wiki/User:SapadorCastelo).
